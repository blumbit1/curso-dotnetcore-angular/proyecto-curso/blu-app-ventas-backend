﻿using POS.Domain.Entities;
using POS.Infrastructure.Commons.Bases.Request;
using POS.Infrastructure.Commons.Bases.Response;

namespace POS.Infrastructure.Persistences.Interfaces
{
    public interface ICategoryRepository
    {
        Task<BaseEntityResponse<Category>> ListCategories(BaseFilterRequest filters);
        Task<IEnumerable<Category>> ListAllCategories();
        Task<Category> GetCategoryById(int id);
        Task<bool> RegisterCategory(Category newCategory);
        Task<bool> EditCategory(Category editCategory);
        Task<bool> DeleteCategory(int id);
    }
}

using POS.Application.Extensions;
using POS.Infrastructure.Extensions;

var builder = WebApplication.CreateBuilder(args);
var Configuration = builder.Configuration;

// Add services to the container.
builder.Services.AddInjectionInfrastructure(Configuration);
builder.Services.AddInjectionApplication(Configuration);

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();

// Habilitar CORS desde el enfoque de .NET Core
builder.Services.AddCors( 
    options => 
    {
        options.AddPolicy(name : "Cors",
            builder => {
                builder.WithOrigins("*");
                builder.AllowAnyMethod();
                builder.AllowAnyHeader();
            });
    });

var app = builder.Build();

// Use CORS
app.UseCors("Cors");

// Configure the HTTP request pipeline.
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();

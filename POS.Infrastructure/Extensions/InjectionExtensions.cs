﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using POS.Infrastructure.Persistences.Contexts;
using POS.Infrastructure.Persistences.Interfaces;
using POS.Infrastructure.Persistences.Repositories;

namespace POS.Infrastructure.Extensions
{
    public static class InjectionExtensions
    {
        public static IServiceCollection AddInjectionInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {

            var assembly = typeof(SisventasContext).Assembly.FullName;

            // Manejo de la conexón hacia la BBDD
            services.AddDbContext<SisventasContext>(options => 
                    options.UseSqlServer(configuration.GetConnectionString("SisventasConnection"), b => b.MigrationsAssembly(assembly)), ServiceLifetime.Transient
                );

            // Adicion del patron UnitOfwork
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Utilities.Static
{
    public static class ReplyMessage
    {
        public const string MESSAGE_QUERY = "Successful query";
        public const string MESSAGE_QUERY_EMPTY = "No records found";
        public const string MESSAGE_SAVE = "Registration successfully";
        public const string MESSAGE_UPDATE = "Updated successfully";
        public const string MESSAGE_DELETE = "Removed successfully";
        public const string MESSAGE_EXISTS = "The record already exists";
        public const string MESSAGE_ACTIVATE = "Registration has been activated";
        public const string MESSAGE_TOKEN = "Successfully generated Token";
        public const string MESSAGE_VALIDATE = "Validation errors";
        public const string MESSAGE_FAILED = "Failed Operation";
    }
}

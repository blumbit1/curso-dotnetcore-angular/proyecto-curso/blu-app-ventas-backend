﻿using FluentValidation;
using POS.Application.Dtos.Category.Request;

namespace POS.Application.Validators.Category
{

    public class CategoryValidator : AbstractValidator<CategoryRequestDto>
    {
        // Constructor
        public CategoryValidator() 
        {
            RuleFor(x => x.Name)
                .NotNull().WithMessage("This field cannot be null, please review")
                .NotEmpty().WithMessage("This field cannot be empty, please review");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using POS.Domain.Entities;
using POS.Infrastructure.Commons.Bases.Request;
using POS.Infrastructure.Commons.Bases.Response;
using POS.Infrastructure.Persistences.Contexts;
using POS.Infrastructure.Persistences.Interfaces;
using POS.Utilities.Static;

namespace POS.Infrastructure.Persistences.Repositories
{
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        private readonly SisventasContext _context;

        public CategoryRepository(SisventasContext context)
        {
            _context = context;
        }

        // CRUD 

        public async Task<BaseEntityResponse<Category>> ListCategories(BaseFilterRequest filters)
        {
            var response = new BaseEntityResponse<Category>();

            // Using LINQ
            var categories = (from c in _context.Categories
                                        where c.AuditDeleteUser == null && c.AuditDeleteDate == null
                                        select c).AsNoTracking().AsQueryable();

            if (filters.NumFilter is not null && !string.IsNullOrEmpty(filters.TextFilter))
            {
                switch (filters.NumFilter) 
                {
                    case 1:
                        categories = categories.Where(x => x.Name!.Contains(filters.TextFilter));
                        break;
                    case 2:
                        categories = categories.Where(x => x.Description!.Contains(filters.TextFilter));
                        break;
                }
            }

            if (filters.StateFilter is not null)
            {
                categories = categories.Where(x => x.State.Equals(filters.StateFilter));
            }

            if (!string.IsNullOrEmpty(filters.StartDate) && !string.IsNullOrEmpty(filters.EndDate))
            {
                categories = categories.Where(x => x.AuditCreateDate >= Convert.ToDateTime(filters.StartDate) && x.AuditCreateDate <= Convert.ToDateTime(filters.EndDate).AddDays(1));
            }

            if (filters.Sort is null) filters.Sort = "CategoryId";

            response.TotalRecords = await categories.CountAsync();
            response.Items = await Ordering(filters, categories, !(bool)filters.Download!).ToListAsync();

            return response;
        }

        public async Task<IEnumerable<Category>> ListAllCategories()
        {
            var categories = await _context.Categories
                .Where(x=> x.State.Equals((int)StateTypes.Active) && x.AuditDeleteUser == null && x.AuditDeleteDate == null).AsNoTracking().ToListAsync();

            return categories;
        }

        public async Task<Category> GetCategoryById(int id)
        {
            var category = await _context.Categories!.AsNoTracking().FirstOrDefaultAsync(x => x.CategoryId.Equals(id));
            return category!;
        }
        
        public async Task<bool> RegisterCategory(Category newCategory)
        {
            newCategory.AuditCreateUser = 1;
            newCategory.AuditCreateDate = DateTime.Now;

            await _context.AddAsync(newCategory);

            var recordsAffected = await _context.SaveChangesAsync();

            return recordsAffected > 0;
        }

        public async Task<bool> EditCategory(Category editCategory)
        {
            editCategory.AuditCreateUser = 1;
            editCategory.AuditUpdateDate = DateTime.Now;

            // Actualizando el registro
            _context.Update(editCategory);
            _context.Entry(editCategory).Property(x => x.AuditCreateUser).IsModified = false;
            _context.Entry(editCategory).Property(x => x.AuditCreateDate).IsModified = false;

            var recordsAffected = await _context.SaveChangesAsync();

            return recordsAffected > 0;
        }

        public async Task<bool> DeleteCategory(int id)
        {
            var deletedCategory = await _context.Categories.AsNoTracking().SingleOrDefaultAsync(x => x.CategoryId.Equals(id));

            // Eliminar el registro

            deletedCategory.AuditDeleteUser = 1;
            deletedCategory.AuditDeleteDate = DateTime.Now;

            // Actualizar los registros
            _context.Update(deletedCategory);

            var recordsAffected = await _context.SaveChangesAsync();

            return recordsAffected > 0;
        }
    }
}
